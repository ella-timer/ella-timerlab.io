import React from 'react';
import './App.css';

const ELLA_TIME_ALLOTMENT = 90

export default class App extends React.Component {
    state = {
        timeRemaining: ELLA_TIME_ALLOTMENT,
        timerCallback: 0,
        ellaTurn: null,
    }

    constructor(props) {
        super(props);

        this.onEllaTurn = this.onEllaTurn.bind(this);
        this.onNotEllaTurn = this.onNotEllaTurn.bind(this);
        this.startTimer = this.startTimer.bind(this);
    }

    startTimer() {
        if (this.state.timeRemaining > 0) {
            this.setState({
                timeRemaining: this.state.timeRemaining - 1,
                timerCallback: setTimeout(this.startTimer, 1000),
            });
        }
    }

    resetTimer() {
        clearTimeout(this.state.timerCallback);
    }

    onEllaTurn() {
        if (!this.state.ellaTurn) {
            this.setState({
                ellaTurn: true,
                timeRemaining: ELLA_TIME_ALLOTMENT,
            });
            this.startTimer();
        }
    }

    onNotEllaTurn() {
        this.setState({
            ellaTurn: false,
            timeRemaining: ELLA_TIME_ALLOTMENT,
        });
        this.resetTimer();
    }

    render() {
        const ella = this.state.ellaTurn ? 'selected' : '';
        const notElla = this.state.ellaTurn === false ? 'selected' : '';
        const ellaOutOfTime = this.state.ellaTurn && this.state.timeRemaining === 0;
        return (
            <div className={"timer " + (ellaOutOfTime ? "out-of-time" : 0)}>
                <div className="buttons">
                    <div className={ella} onClick={this.onEllaTurn}>Ella's Turn</div>
                    <div className={notElla} onClick={this.onNotEllaTurn}>Not Ella's Turn</div>
                </div>
                <div className="countdown">
                    {this.state.timeRemaining}
                </div>
            </div>
        );
    }
}
